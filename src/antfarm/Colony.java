/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm;

import antfarm.ants.*;
import antfarm.gui.AntSimGUI;
import antfarm.gui.ColonyView;
import antfarm.gui.SimulationEvent;
import antfarm.gui.SimulationEventListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author josh
 */
public class Colony implements SimulationEventListener{
    static Colony c=null;
    private int turnCounter;
    final int colonyHeight=27;
    final int colonyWidth=27;
    private LinkedList<ColonyNode> nodeCollection;
    
    private LinkedList<Ant> antCollection;
    public final int MIDNIGHT_VALUE=10;
    private int queenx;
    private int queeny;
    AntSimGUI g;
    ColonyView cv;
    boolean gameOver;
    private ColonyNode qnode;
    public int foodDeposit;
    private ColonyNode bnode;//bala node
    
    
   public static void main(String args[]){
       c = getColony();
       
       c.initColony();
       
   }
    
    private Colony(){
        
        nodeCollection = new LinkedList<>();
        antCollection = new LinkedList<>();
        g = new AntSimGUI();     
        
        
        
    }
    public void initColony(){
        
        
        Ant queen = new Queen();
        cv = new ColonyView(colonyHeight, colonyWidth);
        for(int y=0;y<colonyHeight;y++){
            for(int x=0;x<colonyWidth;x++){
                ColonyNode cn = new ColonyNode(x+","+y);
                
                cn.getView().setID(x+","+y);
                cv.addColonyNodeView(cn.getView(), x, y);

                nodeCollection.add(cn);
                //cn.getView().showNode();
            }
            
        }
        queenx = colonyWidth/2;
        
        queeny = colonyHeight/2;
        
        queen.setLocation(queenx+","+queeny);
        
        antCollection.add(queen);
        
        setQnode(this.colonyNodeSearch(queen.getLocation()));
        setBnode(this.colonyNodeSearch("0,0"));
        if(getQnode()==null){
            System.err.println("No node found for queen:"+queen.getLocation());
            System.exit(0);
        }
        getQnode().setQueen(true);
        getQnode().setFoodAmount(1000);
        getQnode().setOpen(true);
        getQnode().updateViewNode();
        getQnode().getView().showNode();
        Ant a;
        for(int i=0;i<10;i++){
            a= new Soldier();
            addAnt(a);
            //System.out.println(a.toString());
            a=null;
            
        }
        for(int i=0;i<50;i++){
            a=new Forager();
            addAnt(a);
            //System.out.println(a.toString());
            a=null;
            
        }
        for(int i=0;i<4;i++){
            a=new Scout();
            addAnt(a);
            //System.out.println(a.toString());
            a=null;
            
        }
        
    
        g.initGUI(cv);
        g.addSimulationEventListener(this);
        turnCounter=0;
        gameOver=false;
        
               
        
    }
    public void reinitColony(){
        antCollection=new LinkedList<>();
        nodeCollection=new LinkedList<>();
        initColony();
    }
    
    public void turnCtlr(){
        try{
            while(!gameOver)
            turnCtlr(true);
        }catch(Error e){
            for(ColonyNode cn:nodeCollection){
                cn.updateViewNode();
                throw e;
            }
        }
        DebugStuff.debug();
    }
    public void turnCtlr(boolean oneTurn){
        long start=System.currentTimeMillis();
        if(gameOver)
            throw new Error("Game Over");
        if(new Random().nextInt(100)>96){
            addBala();
            
        }
        
        boolean newDay=false;
        if(turnCounter %1000==0){
            System.out.println("turn:"+turnCounter);
            System.out.println("Ant Collection Size:"+antCollection.size());
            System.out.println("Food Deposits:"+foodDeposit);
            DebugStuff.debug();
        }
        if(turnCounter%MIDNIGHT_VALUE==0){
            newDay=true;
            Ant a = Queen.giveBirth();
            for(ColonyNode node: nodeCollection){
                node.setPheremoneLevel(node.getPheremoneLevel()/2);
                node.updateViewNode();
            }
            
            addAnt(a);
            
        }
        
        for(Ant a: antCollection){
            a.takeTurn(newDay);
            if(a instanceof Queen && !a.isAlive()){
                gameOver();
                return;
            }
                
            
        }
        turnCounter++;
        
        buryTheDead();
        long end=System.currentTimeMillis();
        DebugStuff.totTime+=end-start;
        //debug();
    }
    public static Colony getColony(){
        if(c==null)
            c=new Colony();
        return c;
    }

    /**
     * @return the turnCounter
     */
    public int getTurnCounter() {
        return turnCounter;
    }

    /**
     * @param turnCounter the turnCounter to set
     */
    public void setTurnCounter(int turnCounter) {
        this.turnCounter = turnCounter;
    }

    /**
     * @return the nodeCollection
     */
    public LinkedList<ColonyNode> getNodeCollection() {
        return nodeCollection;
    }

    /**
     * @param nodeCollection the nodeCollection to set
     */
    public void setNodeCollection(LinkedList<ColonyNode> nodeCollection) {
        this.nodeCollection = nodeCollection;
    }

    /**
     * @return the queenx
     */
    public int getQueenx() {
        return queenx;
    }

    /**
     * @param queenx the queenx to set
     */
    public void setQueenx(int queenx) {
        this.queenx = queenx;
    }

    /**
     * @return the queeny
     */
    public int getQueeny() {
        return queeny;
    }

    /**
     * @param queeny the queeny to set
     */
    public void setQueeny(int queeny) {
        this.queeny = queeny;
    }

    /**
     * @return the antCollection
     */
    public LinkedList<Ant> getAntCollection() {
        return antCollection;
    }

    /**
     * @param antCollection the antCollection to set
     */
    public void setAntCollection(LinkedList<Ant> antCollection) {
        this.antCollection = antCollection;
    }
    
    public ColonyNode colonyNodeSearch(String searchID){
        long start = System.currentTimeMillis();
        for(ColonyNode node:nodeCollection){
            if(node.getID().equalsIgnoreCase(searchID))
                    return node;
        }
        long end = System.currentTimeMillis();
        DebugStuff.searchNodeTimeCounter+=end-start;
        return null;
    }
    public void addAnt(Ant a){
   
        antCollection.add(a);
        if(a instanceof Soldier)
            getQnode().setSoldierCount(getQnode().getSoldierCount()+1);
        if(a instanceof Scout)
            getQnode().setScoutCount(getQnode().getScoutCount()+1);
        if(a instanceof Forager)
            getQnode().setForagerCount(getQnode().getForagerCount()+1);
        if(a instanceof Bala)
            getQnode().setBalaCount(getQnode().getBalaCount()+1);
        getQnode().updateViewNode();
        a.setID(antCollection.indexOf(a));
        
    }
    public void gameOver(){
        System.out.println("Game Over");
        System.out.println("Food Deposits:"+foodDeposit);
        
        qnode.getView().hideQueenIcon();
        qnode.updateViewNode();
        
        gameOver=true;
    }
    public LinkedList<ColonyNode> getBorders(String location){
        
        LinkedList<ColonyNode> outList = new LinkedList<>();
        String xy[] = location.split(",");
        int x = Integer.parseInt(xy[0]);
        int y = Integer.parseInt(xy[1]);
        //Up Left
        int tempx = x-1;
        int tempy = y-1;
        String tempLoc = tempx + "," + tempy;
        ColonyNode tempNode = colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //Up
        tempx = x;
        tempy = y-1;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //Up Right
        tempx = x+1;
        tempy = y-1;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //Left
        tempx = x-1;
        tempy = y;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //Right
        tempx = x+1;
        tempy = y;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //down left
        tempx = x-1;
        tempy = y+1;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //down
        tempx = x;
        tempy = y+1;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
        //Down right
        tempx = x+1;
        tempy = y+1;
        tempLoc = tempx + "," + tempy;
        tempNode=colonyNodeSearch(tempLoc);
        if(tempNode!=null)
            outList.add(tempNode);
            
        return outList;
    }
    @Override
    public void simulationEventOccurred(SimulationEvent simEvent)
    {
        if (simEvent.getEventType() == SimulationEvent.NORMAL_SETUP_EVENT) {
// set up the simulation for normal operation
            c.initColony();

        } else if (simEvent.getEventType() == SimulationEvent.QUEEN_TEST_EVENT) {
// set up simulation for testing the queen ant
        } else if (simEvent.getEventType() == SimulationEvent.SCOUT_TEST_EVENT) {
// set up simulation for testing the scout ant
        } else if (simEvent.getEventType() == SimulationEvent.FORAGER_TEST_EVENT) {
// set up simulation for testing the forager ant
        } else if (simEvent.getEventType() == SimulationEvent.SOLDIER_TEST_EVENT) {
// set up simulation for testing the soldier ant
        } else if (simEvent.getEventType() == SimulationEvent.RUN_EVENT) {
            System.out.println("Run");
            c.turnCtlr();
        } else if (simEvent.getEventType() == SimulationEvent.STEP_EVENT) {
            System.out.println("Step");
            c.turnCtlr(gameOver);
        } else {

        }
    }
    private void buryTheDead(){
        long start = System.currentTimeMillis();

        Iterator i = antCollection.iterator();
        Ant a = null;
        while(i.hasNext()){
            a=  (Ant)i.next();
            if(!a.isAlive()){
                
                i.remove();
            }
        }
                long end = System.currentTimeMillis();
        DebugStuff.buryTheDeadTimer += end-start;
        
    }
    

    /**
     * @return the qnode
     */
    public ColonyNode getQnode() {
        return qnode;
    }

    /**
     * @param qnode the qnode to set
     */
    public void setQnode(ColonyNode qnode) {
        this.qnode = qnode;
    }
    public void addBala(){
        Bala b = new Bala();
        
        getBnode().setBalaCount(getBnode().getBalaCount()+1);
        getBnode().updateViewNode();
        antCollection.add(b);
        b.setID(antCollection.indexOf(b));
        System.out.println("BALA ID:"+b.getID());
        
    }

    /**
     * @return the bnode
     */
    public ColonyNode getBnode() {
        return bnode;
    }

    /**
     * @param bnode the bnode to set
     */
    public void setBnode(ColonyNode bnode) {
        this.bnode = bnode;
    }

 
    
}
