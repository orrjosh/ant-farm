/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm;

import antfarm.ants.*;

/**
 *
 * @author josh
 */
public class DebugStuff {
    public static long totTime=0;
    public static long searchNodeTimeCounter=0;
    public static long buryTheDeadTimer=0;
    public static long timer;
    
    public static void debug(){
        Colony c = Colony.getColony();
        System.out.println("******************************");
        System.out.println("Colony Size:"+c.getAntCollection().size());
        System.out.println("Turn Counter:"+c.getTurnCounter());
        System.out.println("Tot Time:"+totTime);
        System.out.println("Node Search time"+searchNodeTimeCounter);
        System.out.println("Bury the dead time"+buryTheDeadTimer);
        System.out.println("Method test timer"+buryTheDeadTimer);
        
        int soldierCount=0;
        int scoutCount=0;
        int foragerCount=0;
        int balaCount=0;
        
        for(Ant a:c.getAntCollection()){
            if(a instanceof Soldier)
                soldierCount++;
            if(a instanceof Scout)
                scoutCount++;
            if(a instanceof Forager)
                foragerCount++;
            if(a instanceof Bala)
                balaCount++;
        }
        //System.out.println("QueenAlive:"+c.getAntCollection().peek().isAlive());
        System.out.println("Soldier Count:"+soldierCount+"\n"+
                            "ScoutCount:"+scoutCount+"\n"+
                        "Forager Count:"+foragerCount+"\n"+
                        "Bala Count:"+balaCount+"\n"+
                        "Food Count:"+ c.getQnode().getFoodAmount());
        System.out.println("******************************");
    }
    
}
