/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.Colony;
import antfarm.ColonyNode;
import antfarm.DebugStuff;
import java.util.*;

/**
 *
 * @author josh
 */
public class Forager extends Ant{
    final int FORAGE_MODE=0;
    final int RETURN_MODE=1;
    private int mode=0;
    private Stack<ColonyNode> moveTrail;
    private boolean carryingFood;
    LinkedList<String> pushlog;

    public ColonyNode choosePath(){
//        if(new Random().nextInt(100)>95){
//            System.out.println("---------------------------");
//            System.out.println("Stack log ANT:"+this.getID());
//            for(String log:pushlog){
//                
//                System.out.println(log);
//            }
//        }
        long start = System.currentTimeMillis();    
        Colony c = Colony.getColony();
        ColonyNode retNode=null;
        if(mode==FORAGE_MODE){
            ColonyNode curNode = c.colonyNodeSearch(this.getLocation());
            LinkedList<ColonyNode> borderNodes = c.getBorders(this.getLocation());
            Iterator i = borderNodes.iterator();
            ColonyNode cn=null;
            while(i.hasNext()){
                cn = (ColonyNode)i.next();
                if(!cn.getView().isVisible())
                    i.remove();
            }
                
            LinkedList<ColonyNode> highPheremone = new LinkedList<>();
            i=borderNodes.iterator();
            if(i.hasNext()){
                if(!loopCheck()){
                    highPheremone.add((ColonyNode)i.next());
                    while(i.hasNext()){
                        cn = (ColonyNode)i.next();
                        if(cn.getPheremoneLevel() > highPheremone.peek().getPheremoneLevel()){
                            highPheremone.pop();
                            highPheremone.push(cn);
                        }else if(cn.getPheremoneLevel()==highPheremone.peek().getPheremoneLevel())
                            highPheremone.push(cn);
                    }
                    if(highPheremone.size()>1){
                        highPheremone.remove(curNode);
                    }
                }else highPheremone=borderNodes;
                retNode=highPheremone.get(new Random().nextInt(highPheremone.size()));
            }
            
            if(retNode!=null){
                moveTrail.push(retNode);
                pushlog.push("Loc:"+this.getCurrentLocationNode().getID()+" Mode:"+ this.mode);
            }
        }else if(mode==RETURN_MODE){
            try{
                retNode=moveTrail.pop();
            }catch(Exception e){
                //System.out.println(this.mode);
                System.out.println("ID:"+this.getID());
                System.out.println("Mode:"+this.getMode());
                
                System.out.println("Loc:"+this.getCurrentLocationNode().getID());
                for(String s:pushlog)
                    System.out.println("PushLoc:"+s);
                throw e;
            }
        }else
            throw new Error("Invalid Forager Mode");
        long end = System.currentTimeMillis();
        //DebugStuff.timer += end-start;
        return retNode;
    }
    /**
     * @return the mode
     */
    public Forager(){
        Colony c = Colony.getColony();
        this.setMaxAge(365);
        this.setMode(FORAGE_MODE);
        this.setCarryingFood(false);
        this.setLocation(c.getQueenx()+","+c.getQueeny());
        this.moveTrail=new Stack<>();
        moveTrail.push(c.getQnode());
        this.pushlog=new LinkedList<>();
               
    }
    @Override
    public void takeTurn(boolean newDay){
        super.takeTurn(newDay);
        if(!isAlive()){
            return;
        }
        ColonyNode nextNode = choosePath();
        Colony c = Colony.getColony();
        if(nextNode!=null){
            this.move(nextNode);
        
            if(this.mode==FORAGE_MODE && !(nextNode==c.getQnode())){
                if(nextNode.getFoodAmount()>0){
                    carryingFood=true;
                    nextNode.setFoodAmount(nextNode.getFoodAmount()-1);
                    nextNode.updateViewNode();
                    
                    mode=RETURN_MODE;

                }
            }
        }
        if(this.mode==RETURN_MODE){
            if(nextNode.getID().equalsIgnoreCase(c.getQnode().getID())){
                carryingFood=false;
                nextNode.setFoodAmount(nextNode.getFoodAmount()+1);
                c.foodDeposit++;
                mode=FORAGE_MODE;
                moveTrail.push(nextNode);
                
            }else if(nextNode.getPheremoneLevel()<1000){
                nextNode.setPheremoneLevel(nextNode.getPheremoneLevel()+10);
            }
        }
        if(nextNode!=null)
            nextNode.updateViewNode();
    }
    public int getMode() {
        return mode;
    }


    /**
     * @param mode the mode to set
     */
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * @return the moveTrail
     */
    public Stack getMoveTrail() {
        return moveTrail;
    }

    /**
     * @param moveTrail the moveTrail to set
     */
    public void setMoveTrail(Stack moveTrail) {
        this.moveTrail = moveTrail;
    }

    /**
     * @return the carryingFood
     */
    public boolean isCarryingFood() {
        return carryingFood;
    }

    /**
     * @param carryingFood the carryingFood to set
     */
    public void setCarryingFood(boolean carryingFood) {
        this.carryingFood = carryingFood;
    }
    private boolean loopCheck(){
        long start = System.currentTimeMillis();
        if(moveTrail.size()<10) return false; 
        Stack<ColonyNode> s = (Stack<ColonyNode>)moveTrail.clone();
        LinkedList<String> uniques = new LinkedList<String>();
        while(!s.isEmpty()){
            ColonyNode cn = s.pop();
            if(!uniques.contains(cn.getID())){
                uniques.add(cn.getID());
            }
                
        }
        long end = System.currentTimeMillis();
        //DebugStuff.timer += end-start;
        if(uniques.size()<moveTrail.size()/2) return true;
        else return false;
    }
}
