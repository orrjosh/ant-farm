/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.Colony;
import antfarm.ColonyNode;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author josh
 */
public class Bala extends FightingAnt{
    
    public static final int SCOUT_MODE=0;
    public static final int ATTACK_MODE=1;
    public int mode;
    
    public Bala(){
    Colony c = Colony.getColony();
        this.setAge(0);
        this.setAlive(true);
        this.setMaxAge(365);
        isBala=true;
        this.setLocation("0,0");
        this.mode=SCOUT_MODE;
        
    }
    private ColonyNode choosePath(){
        Colony c = Colony.getColony();
        LinkedList<ColonyNode> borderNodes = c.getBorders(this.getLocation());
        ColonyNode retNode=borderNodes.get(new Random().nextInt(borderNodes.size()));        
        
        return retNode;
    }
    @Override
    public void takeTurn(boolean newDay){
        super.takeTurn(newDay);
        if(!isAlive()){
            return;
        }
        Colony c = Colony.getColony();
        curNode = c.colonyNodeSearch(this.getLocation());
        int antCount = curNode.getForagerCount()+curNode.getScoutCount()+curNode.getSoldierCount();
        if(curNode==c.getQnode()){
            antCount++;
        }
        if(antCount>0){
            this.mode=ATTACK_MODE;
        }else{
            this.mode=SCOUT_MODE;
        }
        if(mode==SCOUT_MODE){
            ColonyNode cn = choosePath();
            if(cn!=null){
                super.move(cn);
            }
        }else{
            
            Ant target = chooseTarget();
            super.attack(target);
        }
    }
    public Ant chooseTarget(){
        LinkedList<Ant> ants;
        ants = this.curNode.getAnts(false);
//        for(Ant a: ants){
//           if(a instanceof Queen){
//               return a;
//           }                                                
//        }
        if(ants.size()>0){
            return ants.get(new Random().nextInt(ants.size()));
        }else{
            throw new Error("Attacking null ant");
        }
        
    }
    
}
