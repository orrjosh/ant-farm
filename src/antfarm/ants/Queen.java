/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.Colony;
import antfarm.ColonyNode;
import java.util.Random;

/**
 *
 * @author josh
 */
public class Queen extends Ant{
    public Queen(){
        super();
        setMaxAge(7300);//20 years
        setID(0);
                
    }
    
    public static Ant giveBirth(){
        int typeInt = new Random().nextInt(100);
        Ant a;
        if(typeInt<50)
            a= new Forager();
        else if(typeInt<75)
            a = new Scout();
        else 
            a = new Soldier();
        return a;
    }
    
    @Override
    public void takeTurn(boolean newDay){
        super.takeTurn(newDay);
        if(!this.isAlive()){
            die();
            return;
        }
        if(!eat()) 
            die();
        
            
        
    }
    
    public boolean eat(){
        Colony c = Colony.getColony();
        ColonyNode node = c.colonyNodeSearch(this.getLocation());
        if(node.getFoodAmount()>0){
            node.setFoodAmount(node.getFoodAmount()-1);
            node.updateViewNode();
            return true;
        }else return false;
        
    }
    @Override
    public void die(){
        Colony.getColony().gameOver();
    }
    
}
