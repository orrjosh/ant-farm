/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.ColonyNode;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author josh
 */
public abstract class FightingAnt extends Ant {
    
    
    public boolean attack(Ant target){
        
        int die = new Random().nextInt(2);
        if(die==1){
            System.out.println("Killed:"+target.getID());
            target.die();
            return true;
        }else return false;
            
    }
    
    
}
