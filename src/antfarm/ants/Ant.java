/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.Colony;
import antfarm.ColonyNode;
import antfarm.DebugStuff;

/**
 *
 * @author josh
 */
public class Ant {
    private int ID;
    private int maxAge;//in days
    private boolean alive;
    private int age;//in days;
    private String location;//corrosponds to ColonyNode.id
    protected ColonyNode curNode;
    public boolean isBala;

    public Ant(){
        
        
        Colony c = Colony.getColony();
        age=0;
        alive=true;
        this.maxAge=365;
        isBala=false;
        
        this.setLocation(c.getQueenx() + "," + c.getQueeny());

    }
    public void die(){
        
        alive=false;
        ColonyNode curNode = Colony.getColony().colonyNodeSearch(location);
        if(this instanceof Queen){
            Colony.getColony().gameOver();
            return;
        }
        if(this instanceof Soldier)
            curNode.setSoldierCount(curNode.getSoldierCount()-1);
        if(this instanceof Scout)
            curNode.setScoutCount(curNode.getScoutCount()-1);
        if(this instanceof Forager)
            curNode.setForagerCount(curNode.getForagerCount()-1);
        if(this instanceof Bala)
            curNode.setBalaCount(curNode.getBalaCount()-1);
        
    }
    public void takeTurn(boolean newDay){
        if(newDay){
            
            age++;
        }
        if(age>=maxAge){
            die();
            
        }
        
        
    }
    public void move(ColonyNode nextNode){
        Colony c = Colony.getColony();
        ColonyNode curNode = c.colonyNodeSearch(location);
        if(this instanceof Scout){
            curNode.setScoutCount(curNode.getScoutCount()-1);
            nextNode.setScoutCount(nextNode.getScoutCount()+1);
        }
        if(this instanceof Soldier){
            curNode.setSoldierCount(curNode.getSoldierCount()-1);
            nextNode.setSoldierCount(nextNode.getSoldierCount()+1);
        }
        if(this instanceof Forager){
            curNode.setForagerCount(curNode.getForagerCount()-1);
            nextNode.setForagerCount(nextNode.getForagerCount()+1);
        }
        if(this instanceof Bala){
            curNode.setBalaCount(curNode.getBalaCount()-1);
            nextNode.setBalaCount(nextNode.getBalaCount()+1);
        }
        
        location=nextNode.getID();
        curNode.updateViewNode();
        nextNode.updateViewNode();
        
    }
    
    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * @return the maxAge
     */
    public int getMaxAge() {
        return maxAge;
    }

    /**
     * @param maxAge the maxAge to set
     */
    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    /**
     * @return the alive
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     * @param alive the alive to set
     */
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }
    public ColonyNode getCurrentLocationNode(){
        Colony c = Colony.getColony();
        return c.colonyNodeSearch(location);
    }
    
   
    
    
}
