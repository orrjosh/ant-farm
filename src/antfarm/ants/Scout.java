/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.Colony;
import antfarm.ColonyNode;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author josh
 */
public class Scout extends Ant {
    private ColonyNode choosePath(){
        Colony c = Colony.getColony();
        LinkedList<ColonyNode> borderNodes = c.getBorders(this.getLocation());
        ColonyNode retNode=borderNodes.get(new Random().nextInt(borderNodes.size()));        
        
        return retNode;
    }
    private int lookForFood(){
        if(new Random().nextInt(100)<25){
            int food = new Random().nextInt(500);
            return food+500;
        }else return 0;
      
    }
    @Override
    public void takeTurn(boolean newDay){
        
        Colony c = Colony.getColony();
        super.takeTurn(newDay);
        if(!this.isAlive())
            return;
        
        ColonyNode nextNode = choosePath();
        move(nextNode);
        ColonyNode curNode = c.colonyNodeSearch(this.getLocation());
        if(!curNode.isOpen()){
            curNode.setFoodAmount(lookForFood());
            curNode.setOpen(true);
        }
        
            
    }
}
