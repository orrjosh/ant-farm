/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm.ants;

import antfarm.Colony;
import antfarm.ColonyNode;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/** 
 *
 * @author josh
 */
public class Soldier extends FightingAnt {
    public static final int SCOUT_MODE=0;
    public static final int ATTACK_MODE=1;
    private ColonyNode curNode;
    private int mode;
    
    public Soldier(){
        super();
        mode=SCOUT_MODE;
    }
    @Override
    public void takeTurn(boolean newDay){
        super.takeTurn(newDay);
        if(!isAlive()){
            return;
        }
        Colony c = Colony.getColony();
        curNode = c.colonyNodeSearch(this.getLocation());
        if(curNode.getBalaCount()>0){
            this.mode=ATTACK_MODE;
        }else{
            this.mode=SCOUT_MODE;
        }
        if(mode==SCOUT_MODE){
            ColonyNode cn = choosePath();
            if(cn!=null){
                super.move(cn);
            }
        }else{
            System.out.println("ATTACK MODE");
            Ant target = chooseTarget();
            super.attack(target);
        }
        
    }
    public Ant chooseTarget(){
        LinkedList<Ant> ants;
        ants = this.curNode.getAnts(true);
        if(ants.size()>0){
            return ants.get(new Random().nextInt(ants.size()));
        }else{
            throw new Error("Attacking null ant\nBalaCount:"+curNode.getBalaCount());
            
        }
        
    }
    public ColonyNode choosePath(){
        Colony c = Colony.getColony();
        LinkedList<ColonyNode> borderNodes = c.getBorders(this.getLocation());
        LinkedList<ColonyNode> balaNodes = new LinkedList<>();
        
        Iterator i = borderNodes.iterator();
        while(i.hasNext()){
            ColonyNode cn = (ColonyNode)i.next();
            if(!cn.getView().isVisible()){
                i.remove();
                continue;
            }
            if(cn.getBalaCount()>0){
                balaNodes.add(cn);
            }
        }
        ColonyNode retNode;
        if(balaNodes.size()>0){
            retNode = balaNodes.get(new Random().nextInt(balaNodes.size()));
        }else if(borderNodes.size()>0){
            retNode = borderNodes.get(new Random().nextInt(borderNodes.size()));
        }else {retNode=null;}
        return retNode;
    }
    
    

    /**
     * @return the mode
     */
    public int getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    public void setMode(int mode) {
        this.mode = mode;
    }
    
    
}
