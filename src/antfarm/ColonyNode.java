/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package antfarm;

import antfarm.ants.Ant;
import antfarm.ants.Bala;
import antfarm.gui.ColonyNodeView;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author josh
 */
public class ColonyNode {
    private String ID;
    private boolean queen;
    private int foragerCount;
    private int scoutCount;
    private int soldierCount;
    private int balaCount;
    private int foodAmount;
    private int pheremoneLevel;
    private boolean open;
    private ColonyNodeView view;
    
    public void updateViewNode(){
        view.setQueen(queen);
        view.setForagerCount(foragerCount);
        view.setScoutCount(scoutCount);
        view.setSoldierCount(soldierCount);
        view.setBalaCount(balaCount);
        view.setFoodAmount(foodAmount);
        view.setPheromoneLevel(pheremoneLevel);
        
        if(queen)
            view.showQueenIcon();
        if(foragerCount>0) view.showForagerIcon();
        else view.hideForagerIcon();
        if(scoutCount>0) view.showScoutIcon();
        else view.hideScoutIcon();
        if(soldierCount>0) view.showSoldierIcon();
        else view.hideSoldierIcon();
        if(balaCount>0) view.showBalaIcon();
        else view.hideBalaIcon();
        
        
    }
    
    
    public ColonyNode(String ID){
        this.ID=ID;
        view = new ColonyNodeView();
        view.setID(ID);
        open=false;
    }

    
    /**
     * @return the queen
     */
    public boolean isQueen() {
        return queen;
    }

    /**
     * @param queen the queen to set
     */
    public void setQueen(boolean queen) {
        this.queen = queen;
    }

    /**
     * @return the foragerCount
     */
    public int getForagerCount() {
        return foragerCount;
    }

    /**
     * @param foragerCount the foragerCount to set
     */
    public void setForagerCount(int foragerCount) {
        if(!this.view.isVisible())
            throw new Error("Forager on invisible square"+this.getID());
        this.foragerCount = foragerCount;
    }

    /**
     * @return the scountCount
     */
    public int getScoutCount() {
        return scoutCount;
    }

    /**
     * @param scountCount the scountCount to set
     */
    public void setScoutCount(int scoutCount) {
        this.scoutCount = scoutCount;
    }

    /**
     * @return the soldierCount
     */
    public int getSoldierCount() {
        return soldierCount;
    }

    /**
     * @param soldierCount the soldierCount to set
     */
    public void setSoldierCount(int soldierCount) {
        if(!this.view.isVisible())
            throw new Error("Soldier on invisible square"+this.getID());
        this.soldierCount = soldierCount;
    }

    /**
     * @return the balaCountForager 
     */
    public int getBalaCount() {
        return balaCount;
    }

    /**
     * @param balaCount the balaCount to set
     */
    public void setBalaCount(int balaCount) {
        this.balaCount = balaCount;
    }

    /**
     * @return the foodAmount
     */
    public int getFoodAmount() {
        return foodAmount;
    }

    /**
     * @param foodAmount the foodAmount to set
     */
    public void setFoodAmount(int foodAmount) {
        this.foodAmount = foodAmount;
    }

    /**
     * @return the pheremoneLevel
     */
    public int getPheremoneLevel() {
        return pheremoneLevel;
    }

    /**
     * @param pheremoneLevel the pheremoneLevel to set
     */
    public void setPheremoneLevel(int pheremoneLevel) {
        this.pheremoneLevel = pheremoneLevel;
    }

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }
    
    

    /**
     * @return the view
     */
    public ColonyNodeView getView() {
        return view;
    }

    /**
     * @param view the view to set
     */
    public void setView(ColonyNodeView view) {
        this.view = view;
    }

    /**
     * @return the open
     */
    public boolean isOpen() {
        return open;
    }

    /**
     * @param open the open to set
     */
    public void setOpen(boolean open) {
        this.open = open;
        if(open)
            view.showNode();
        else
            view.hideNode();
        
        
    }
    public LinkedList<Ant> getAnts(boolean balaOnly) {
        //System.out.println("AttackNode:"+ID);
        Colony c = Colony.getColony();
        LinkedList<Ant> ants = c.getAntCollection();
        LinkedList<Ant> attackableAnts = new LinkedList<>();
        //System.out.println("AttackFullSize:" + ants.size());
        Iterator i = ants.iterator();
        while (i.hasNext()) {
            Ant a = (Ant) i.next();

            if (a.getLocation().equalsIgnoreCase(this.ID)) {
                attackableAnts.add(a);

            }
        }
         i = attackableAnts.iterator();
        //System.out.println("AttackPreFilterSize:" + ants.size());
        while (i.hasNext()) {
            
            Ant b = (Ant) i.next();
            //System.out.println("B is bala"+b.isBala + " BalaOnly:"+balaOnly);
            if (balaOnly) {
                if (b.isBala==false) {
                    i.remove();
                    
                }
            } else {
                if (b.isBala==true) {
                    i.remove();
                    //System.out.println("b ID:"+b.getID());
                }
            }

        }
        

        return attackableAnts;

    }
    
    
}
